FROM fabric8/java-alpine-openjdk8-jdk:latest
LABEL maintainer="lixl@live.com"

ADD target/*.jar /app.jar
EXPOSE 8090

CMD ["java","-jar","/app.jar"]